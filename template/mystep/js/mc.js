// "MC" library of helper functions

var MC = (function(){
	
	var MC = {};
	
	// dependencies on other template variables
	
	MC.submit_button = button;
	MC.website = website;
	
	MC.getParameterByName = function(name, url) {
		// extract url parameter
		// from http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
	    if (!url) {
	      url = window.location.href;
	    }
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	    results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
	MC.saveToVariable = function(variableName, variableValue, callback) {
		MC.website.restCall("variable/write/" + variableName, variableValue, , null);
	}
	
	// TODO: test getVariablesFromUrl
	MC.getVariablesFromUrl = function(map){
		// example: getVariablesFromUrl({"id_parameter": "id_variable"})
		var requestsToComplete = 0;
		$("#form_submit").attr("action", window.location.pathname); // make sure we ignore parameters when submitting
		for (var key in map){
			var value = MC.getParameterByName(key);
			if (value != null){
				requestsToComplete++;
				MC.saveToVariable(map[key], value, function (response) {
					requestsToComplete--;
					if (requestsToComplete === 0){
						MC.submit_button.submit(); // go to next slide
					}
				});
			}
		}
	}

	return MC;
})();