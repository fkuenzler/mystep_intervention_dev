// Helpers
var log = function (value) {
    if (config.debug) {
        console.log(value);
    }
};
/*
 
 Possible layout options as classes for the "all-wrapper" are:
 
 Directly settable optional layout attributes: 
 
 * logo
 * eyecatcher
 * sex-1-1 ... -9 (male)
 * sex-2-1 ... -11 (female)
 
 * sex-1-20 ... -23 (male)
 * sex-2-20 ... -23 (female)
 
 
 Internally managed:
 
 * button-next
 * button-previous
 
 Screening Survey options, set by script based on combination of layout and optional layout attribute:
 
 * part (layout text_only not type-bottom-scroll or type-bottom-center)
 * text-full (layout text_only)
 * text-bottom-scroll (layout text_only and type-bottom-scroll)
 * text-bottom-center (layout text_only and type-bottom-center)
 * title (several)
 * media-bottom (layout graph_only)
 * media-full  (layout media_only and type-media-full)
 * select-one-lickert-3-6 (layout select_one)
 * select-one-text-1-multi (layout select_one and type-text-1-multi)
 * select-one-text-2 (layout select_one and type-text-2)
 * select-one-text-2-big-without-numbers (layout select_one and type-text-2-big-without-numbers)
 * select-one-text-2-multi (layout select_one and type-text-2-multi)
 * select-one-text-3 (layout select_one and type-text-3)
 * select-one-text-3-big (layout select_one and type-text-3-big)
 * select-one-text-4 (layout select_one and type-text-4)
 * select-one-text-5 (layout select_one and type-text-5)
 * select-one-text-3-9 (layout select_one and type-text-4-plus)
 * text-box (layout text_input)
 * text-box-multiline (layout multiline_text_input)
 * number-box (layout number_input)
 * number-drinks-selection (layout number_input and type-drinks)
 * password-box (layout password_input)
 
 Feedback options, set by script based on combination of layout and optional layout attribute:
 
 * part (layout text_only not type-bottom-scroll or type-bottom-center)
 * text-full (several)
 * text-bottom-scroll (type-bottom-scroll)
 * text-bottom-center (type-bottom-center)
 * title (several)
 * title-feedback-text (type image)
 * media-bottom (type-media)
 * media-full  (type-media-full)
 
 */
// Website object

var _lang = "";
var _maxEntries = 16;
var _currEntries = 0;
var _updateTime = "50000";
var _challengeType = "";
var _language;
var _posted = false;
var _status = "";
var _dropzone;
var _challengeIdentifier = "";
var _timeoutSet = false;
var _voted = false;

var prices = {
    "1": 9,
    "2": 20,
    "3": 0,
    "4": 5,
    "5": 4
};

var website = {
    eyecatcher: function () {
        var suffix = website.getRestOfParameterBeginningWith("image-");

        //set eyecatcher
        var doc_width = $(document).width();

        if (doc_width > 991) {
            $("#logo-area .pull-right").addClass("hidden-desktop");
            $(".eyecatcher").show();
            $(".eyecatcher img").attr("src", "img/css-based/" + suffix + ".png");
        } else {
            log("hide");
            $(".eyecatcher").hide();
            $("#logo-area .pull-right").removeClass("hidden-desktop");
        }
    },
    init: function () {
        if (config.questionsCount > 1) {
            $("#title-top").html($("#title").html());
            $("#title").html("");
        }

        $("input[type=text]").keyup(function () {

            if ($(this).val() != "") {
                $(this).css("color", "#333");
            } else {
                $(this).css("color", "white");
            }

            return true;
        })

        $("textarea").keyup(function () {
            if ($(this).parent().hasClass("content")) {
                return true;
            }
            if ($(this).val() != "") {
                $(this).css("color", "#333");
            } else {
                $(this).css("color", "white");
            }

            return true;
        });

        var lickert_length = $(".lickert-scala li").length;
        $(".likert li").width(100 / lickert_length);

        // Cleanup of attributes
        config.cleanAttributes = config.attributes.trim().replace(/\s{2,}/g, ' ').split(" ");

        // Preventing form field autocomplete
        log("Preventing form field autocomplete browser problem...");
        $(":input:not([type=hidden])").attr("autocomplete", "off");
        $(":input:not([type=hidden])").attr("readonly", true);
        $(":input:not([type=hidden])").removeAttr('readonly');

        // Eyecatcher images
        if (website.checkStart("image-")) {
            website.eyecatcher();
        }

        // Hide navigation under specific conditions
        if ((config.isFeedback && config.isFirstSlide && config.isLastSlide) ||  (!config.isFeedback && config.isLastSlide)) {
            log("Hide navigation");
            $("#nav").css("display", "none");
        }

        if (config.isFeedback) {
            log("Is feedback");
            if (website.check("type-bottom-scroll")) {
                website.activate("title");
                website.activate("text-bottom-scroll");
            } else if (website.check("type-bottom-center")) {
                website.activate("title");
                website.activate("text-bottom-center");
            } else if (website.check("type-media-full")) {
                website.activate("media-full");
                website.activate("part");
            } else if (website.check("type-media")) {
                website.activate("title_feedback_text");
                website.activate("media-bottom");
            } else if (website.check("graph")) {
                website.activate("graph");
            } else {
                website.activate("text-full");
                website.activate("part");
            }
            website.activate("logo");
        } else {
            log("Number of questions in slide: " + config.questionsCount);
            if (website.layout("select_many")) {
                website.activate("select-many");
            }
            // text_only
            if (website.layout("text_only")) {
                if (website.check("type-bottom-scroll")) {
                    website.activate("title");
                    website.activate("text-bottom-scroll");
                } else if (website.check("type-bottom-center")) {
                    website.activate("title");
                    website.activate("text-bottom-center");
                } else {
                    website.activate("text-full");
                    website.activate("part");
                }
                website.allowNext(true);
            }

            // media_only
            if (website.layout("media_only")) {
                if (website.check("type-media-full")) {
                    website.activate("media-full");
                    website.activate("part");
                } else {
                    website.activate("title");
                    website.activate("media-bottom");
                }
                website.allowNext(true);
            }

            // Cheat for media challenges
            if (website.layout("media_only")) {
                $("#feedback-box").css("display", "none");
            }
            
            // Special solution for Offline-Server
            if ($("#feedback-link") != null && $("#feedback-link").attr("href") != null && $("#feedback-link").attr("href").indexOf("https://OFFLINE/") == 0) {
                $("#feedback-link").attr("href",$("#feedback-link").attr("href").replace("OFFLINE","192.168.0.1:8443/MC/surveys-short"));
            }

            // select_one
            if (website.layout("select_one")) {
                if (website.check("type-text-2")) {
                    website.activate("title");
                    website.activate("select-one-text-2");
                } else if (website.check("type-text-1-multi")) {
                    website.activate("title-top");
                    website.activate("part");
                    website.activate("select-one-text-1-multi");
                    $(".result_variables").each(function () {
                        $(this).val("0");
                    });
                } else if (website.check("type-text-2-multi")) {
                    website.activate("title-top");
                    website.activate("part");
                    website.activate("select-one-text-2-multi");
                } else if (website.check("type-pic-2-big-without-numbers")) {
                    website.activate("title");
                    website.activate("select-one-pic-2-big-without-numbers");
                } else if (website.check("type-text-2-big-without-numbers")) {
                    website.activate("title");
                    website.activate("select-one-text-2-big-without-numbers");
                } else if (website.check("type-text-3")) {
                    website.activate("title");
                    website.activate("select-one-text-3");
                } else if (website.check("type-text-3-big")) {
                    website.activate("title");
                    website.activate("select-one-text-3-big");
                } else if (website.check("type-text-4")) {
                    website.activate("title");
                    website.activate("select-one-text-4");
                } else if (website.check("type-text-5")) {
                    website.activate("title");
                    website.activate("select-one-text-5");
                } else if (website.check("type-text-4-plus")) {
                    website.activate("title");
                    website.activate("select-one-text-3-9");
                } else if (website.check("type-slider")) {
                    website.activate("title");
                    website.activate("select-one-slider");
                    $(".result_variables").each(function () {
                        $(this).val($("#select-one-slider").attr("default-value") / 2);
                    });
                    website.allowNext(true);
                } else {
                    website.activate("title");
                    website.activate("select-one-lickert-3-6");
                }
            }

            if (website.layout("number_input")) {
                website.activate("number-box");
            }

            // Set focus based on layout
            if (website.layout("number_input")) {
                $("#number-box input").focus();
            } else if (website.layout("text_input")) {
                $("#text-box input").focus();
            } else if (website.layout("multiline_text_input")) {
                $("#text-box-multiline textarea").focus();
            }
        }

        log("Register key handler...");

        document.onkeypress = button.keyNext;
        if (document.layers) {
            document.captureEvents(Event.KEYPRESS);
        }

        if (!config.isFeedback && config.validationErrorMessage != "") {
            $.Zebra_Dialog("<span class='message with-linebreaks'>" + config.validationErrorMessage + "</span>", {
                "type": "warning",
                "title": "PathMate2"
            });
        }

        log("Init done.");
    },
    translate: function (callback_function) {

        _lang = $("html").attr("lang");
        // Logo
        if (_lang == "fr-CH") {
            $("img.logo_de").css("display", "none");
        } else {
            $("img.logo_fr").css("display", "none");
        }
        var _baseUrl = $("base").attr("href");
        //get the json_file
        $.getJSON(_baseUrl + "lang/" + _lang + ".json", function (data) {
                _language = data;
                $(".lang").each(function () {
                    var key = $(this).attr("data-lang").trim();
                    if ($(this).is("img")) {
                        $(this).attr("src", website.translateKey(key, data));
                    }

                    if ($(this).is("input")) {
                        $(this).val(website.translateKey(key, data));
                    } else if ($(this).is("textarea")) {
                        $(this).attr("placeholder", website.translateKey(key, data));
                    } else {
                        $(this).html(website.translateKey(key, data));
                    }

                });
                callback_function();
            })
            .fail(function () {
                console.log("could not load language file");
            })

    },
    translateKey: function (key, data) {

        if (data.hasOwnProperty(key)) {
            return data[key];
        } else {

            return key;
        }

    },
    getCorrectField: function (userValue, appropriateGraphValues) {
        if (Math.floor(userValue) == Math.floor(appropriateGraphValues.low)) {
            return 0;
        } else if (Math.floor(userValue) == Math.floor(appropriateGraphValues.mid)) {
            return 1;
        }
        return 2;
    },
    fillTrafficLight: function () {
        website.restCall("variable/readMany/ampelBeHealthyAlkoholkonsum,ampelBeHealthyCannabiskonsum,ampelBeHealthySport,ampelBeHealthyTabakkonsum,ampelBeSmartAndereZugehen,ampelBeSmartBeduerfnisse,ampelBeSmartGruppendruck,ampelBeYouAnderePersonen,ampelBeYouFreizeit,ampelBeYouLehre", null, function (data) {
            log(data);

            website.setTrafficField("ampelBeHealthyAlkoholkonsum", data.variables[0].value);
            website.setTrafficField("ampelBeHealthyCannabiskonsum", data.variables[1].value);
            website.setTrafficField("ampelBeHealthySport", data.variables[2].value);
            website.setTrafficField("ampelBeHealthyTabakkonsum", data.variables[3].value);

            website.setTrafficField("ampelBeSmartAndereZugehen", data.variables[4].value);
            website.setTrafficField("ampelBeSmartBeduerfnisse", data.variables[5].value);
            website.setTrafficField("ampelBeSmartGruppendruck", data.variables[6].value);

            website.setTrafficField("ampelBeYouAnderePersonen", data.variables[7].value);
            website.setTrafficField("ampelBeYouFreizeit", data.variables[8].value);
            website.setTrafficField("ampelBeYouLehre", data.variables[9].value);
        });
    },
    setTrafficField: function (field, value) {
        if (field == "ampelBeHealthyAlkoholkonsum") {
            website.adjustTrafficField("MC_AKOHOLKONSUM", value);
        } else if (field == "ampelBeHealthyCannabiskonsum") {
            website.adjustTrafficField("MC_CANABISKONSUM", value);
        } else if (field == "ampelBeHealthySport") {
            website.adjustTrafficField("MC_SPORT", value);
        } else if (field == "ampelBeHealthyTabakkonsum") {
            website.adjustTrafficField("MC_TABAKKONSUM", value);
        } else if (field == "ampelBeSmartAndereZugehen") {
            website.adjustTrafficField("MC_AUF_ANDERE_ZUGEHEN", value);
        } else if (field == "ampelBeSmartBeduerfnisse") {
            website.adjustTrafficField("MC_BEDUERFNISSE_AUSDRUECKEN", value);
        } else if (field == "ampelBeSmartGruppendruck") {
            website.adjustTrafficField("MC_GRUPPENDRUCK_AUSHALTEN", value);
        } else if (field == "ampelBeYouAnderePersonen") {
            website.adjustTrafficField("MC_MIT_ANDEREN_PERSONEN", value);
        } else if (field == "ampelBeYouFreizeit") {
            website.adjustTrafficField("MC_FREIZEIT", value);
        } else if (field == "ampelBeYouLehre") {
            website.adjustTrafficField("MC_LEHRE", value);
        }
    },
    adjustTrafficField: function (matcher, value) {
        log("Set " + matcher + " to " + value);
        switch (value) {
        case "1":
            $("span[data-lang='" + matcher + "'] + i").addClass("green");
            break;
        case "2":
            $("span[data-lang='" + matcher + "'] + i").addClass("yellow");
            break;
        case "3":
            $("span[data-lang='" + matcher + "'] + i").addClass("red");
            break;
            /*
            case "1":
                $("span[data-lang='" + matcher + "'] ~ i[class~='green']").removeClass("fa-circle-thin");
                $("span[data-lang='" + matcher + "'] ~ i[class~='green']").addClass("fa-circle");
                break;
            case "2":
                $("span[data-lang='" + matcher + "'] ~ i[class~='yellow']").removeClass("fa-circle-thin");
                $("span[data-lang='" + matcher + "'] ~ i[class~='yellow']").addClass("fa-circle");
                break;
            case "3":
                $("span[data-lang='" + matcher + "'] ~ i[class~='red']").removeClass("fa-circle-thin");
                $("span[data-lang='" + matcher + "'] ~ i[class~='red']").addClass("fa-circle");
                break;
            */
        }
    },
    drawGraph: function () {

        if ($(".graph").length <= 0) {
            return false;
        }

        var stressgraph = [
            {
                age: 15,
                values: [{
                    sex: 1,
                    values: {
                        low: 25,
                        mid: 62.5,
                        high: 12.5
                    }
                }, {
                    sex: 2,
                    values: {
                        low: 13.3,
                        mid: 46.7,
                        high: 40
                    }
                }]
            },
            {
                age: 16,
                values: [{
                    sex: 1,
                    values: {
                        low: 26,
                        mid: 53.1,
                        high: 20.9
                    }
                }, {
                    sex: 2,
                    values: {
                        low: 20.5,
                        mid: 46.8,
                        high: 32.7
                    }
                }]
            },
            {
                age: 17,
                values: [{
                    sex: 1,
                    values: {
                        low: 15.6,
                        mid: 59.8,
                        high: 24.6
                    }
                }, {
                    sex: 2,
                    values: {
                        low: 13.7,
                        mid: 44.6,
                        high: 41.6
                    }
                }]
            },
            {
                age: 18,
                values: [{
                    sex: 1,
                    values: {
                        low: 14.6,
                        mid: 62.5,
                        high: 22.9
                    }
                }, {
                    sex: 2,
                    values: {
                        low: 12.6,
                        mid: 52.3,
                        high: 35.1
                    }
                }]
            },
            {
                age: 19,
                values: [{
                    sex: 1,
                    values: {
                        low: 23.7,
                        mid: 52.5,
                        high: 23.7
                    }
                }, {
                    sex: 2,
                    values: {
                        low: 14.3,
                        mid: 51.8,
                        high: 33.9
                    }
                }]
            }
        ];

        website.restCall("variable/readMany/sexT0,ageT0,prozentstressT0", null, function (data) {
            log(data);

            var age = data.variables[1].value;
            var sex = data.variables[0].value - 1;

            if (age > 19) {
                age = 19;
            } else if (age < 15) {
                age = 15;
            }

            var values = {};
            $.each(stressgraph, function (i, item) {


                if (item.age == age) {
                    //male
                    values = item.values[sex].values;
                }
            })

            var field = website.getCorrectField(data.variables[2].value, values);
            var pull, line;

            switch (field) {
            case 0:
                pull = [0.2, 0.0, 0.0];
                break;
            case 1:
                pull = [0.0, 0.2, 0.0];
                break;
            case 2:
                pull = [0.0, 0.0, 0.2];
                break;
            }
            var d3 = Plotly.d3;
            var WIDTH_IN_PERCENT_OF_PARENT = 80,
                HEIGHT_IN_PERCENT_OF_PARENT = 50;
            var gd3 = d3.select('#innerGraph')
                .style({
                    width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                    'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
                    height: HEIGHT_IN_PERCENT_OF_PARENT + '%',
                    'margin-top': 0,
                });
            var gd = gd3.node();
            var data = [{
                values: [values.low, values.mid, values.high],
                labels: [website.translateKey("MC_LITTLE", _language), website.translateKey("MC_MIDDLE", _language), website.translateKey("MC_HIGH", _language)],
                type: 'pie',
                pull: pull,
                showlegend: false,
                textinfo: "label+percent",
                hoverinfo: "none",
                textfont: {
                    size: 16
                },
                marker: {
                    colors: ["#28d142", "#ffe30b", "#e03c34"],
                    line: {
                        color: "#fff",
                        width: "2"
                    }
                },
                outsidetextfont: {
                    color: "#fff"
                },
                insidetextfont: {
                    color: "#fff"
                },
                direction: 'clockwise',
                rotation: 0,
                sort: false
                }];
            var layout = {
                plot_bgcolor: "transparent",
                paper_bgcolor: 'transparent',
                margin: {
                    b: 0,
                    l: 0,
                    r: 0,
                    t: 0
                }
            };
            Plotly.newPlot(gd, data, layout, {
                displayModeBar: false
            });
            window.onresize = function () {
                Plotly.Plots.resize(gd);
            };
        })


    },
    restCall: function (command, postData, callback, dataType) {
        log("REST call " + command + " " + (postData == null) + " " + (callback == null));
        $.ajax({
            type: postData == null ? "GET" : "POST",
            data: postData,
            dataType: typeof(dataType) == "undefined" ? "json" : dataType,
            contentType: postData == null ? "application/json; charset=UTF-8" : "text/plain; charset=UTF-8",
            beforeSend: function (request) {
                request.setRequestHeader("token", config.token);
            },
            url: config.rest + command,
            success: function (data) {
                if (callback != null) {
                    callback(data);
                };
            },
            error: function (xhr, exception) {

                if (xhr.status == 406 && !_timeoutSet) {

                    _timeoutSet = true;
                    $.Zebra_Dialog("<span class='message with-linebreaks'>" + website.translateKey("MC_RELOAD_PAGE", _language) + "</span>", {
                        "type": "warning",
                        "title": "PathMate2"
                    });
                    return false;
                }
                log(exception);
                log(command + " --> " + xhr.status + " (" + xhr.statusText + "): " + xhr.responseText);
            }
        })
    },
    initChallenge: function () {
        //get the identifier
        _challengeIdentifier = website.getRestOfParameterBeginningWith("challenge-");

        if ($("#all-wrapper").hasClass("text")) {
            _challengeType = "text";

            $(".add-info").remove();
        } else {
            _challengeType = "image";
        }


        website.restCall("variable/readMany/participantName,labelGroup", null, function (data) {
            $(".username").html(data.variables[0].value);
            website.restCall("variable/calculateGroupAverage/points", null, function (points) {

                website.setRank(parseInt(points.valueOfParticipant) * config.pointsMultiplicator);
            })


        });

        website.initDropzone();

        var restString = "variable/readMany/challengeStatus" + _challengeIdentifier + ",challengeComment" + _challengeIdentifier;

        if (_challengeType == "image") {
            restString += ",challengeImage" + _challengeIdentifier;

        }
        website.restCall(restString, null, function (data) {
            log("Challenge type: " + _challengeType);

            //var description = data.variables[0].value.split("|");
            var is_posted = false;
            if (_challengeType == "text" && data.variables[1].value != "") {
                is_posted = true;
            }

            var url = "";
            if (_challengeType == "image" && data.variables[2].value != "") {
                //there is an image
                is_posted = true;
                url = data.variables[2].value;
            }

            /*
            var desc = "";
            if (_lang == "de-CH") {
                desc = description[0];
            } else {
                desc = description[1];
            }

            $(".challengeDescription").html(desc);
            */

            website.setStatus(data.variables[0].value, is_posted);

            if (_status != "active") {
                if (!_posted) {
                    //No_entry

                    $(".entry").addClass(".no-entry").html(website.translateKey("MC_NO_ENTRY", _language));
                }
            }

            if (_posted) {
                if (_challengeIdentifier < 20) {
                    website.restCall("voting/votings/challengeVotes" + _challengeIdentifier, null, function (votings) {
                        website.setPosting(data.variables[1].value, votings.value, url);
                    });
                } else {
                    website.setPosting(data.variables[1].value, 0, url);
                    $("#challengeOverview").addClass("own");
                }
            } else {
                website.setPosting(data.variables[1].value, 0, url);
            }

            if (_status != "active" && _status != "checking") {
                var add_string = "";
                if (_challengeType == "image") {
                    add_string += ",challengeImage" + _challengeIdentifier;
                }

                website.restCall("variable/readGroupArrayMany/challengeComment" + _challengeIdentifier + add_string + ",challengeNotAppropriate" + _challengeIdentifier, null, function (data) {

                    website.restCall("voting/votingsGroupArray/challengeVotes" + _challengeIdentifier, null, function (votes) {


                        var entries = website.mergeArrays(data, votes);

                        website.setEntries(entries);

                        if (_status != "finished") {
                            window.setInterval(function () {
                                //insert RestCall with this function as Callback
                                website.restCall("variable/readGroupArrayMany/challengeComment" + _challengeIdentifier + add_string + ",challengeNotAppropriate" + _challengeIdentifier, null, function (data) {

                                    website.restCall("voting/votingsGroupArray/challengeVotes" + _challengeIdentifier, null, function (votes) {

                                        website.restCall("voting/votings/challengeVotes" + _challengeIdentifier, null, function (votings) {
                                            log(votings.value);
                                            $("#myState .count").html(votings.value);
                                        })
                                        var entries = website.mergeArrays(data, votes);
                                        console.log(entries)
                                        website.setEntries(entries);

                                    })
                                })
                            }, _updateTime);
                            website.setVoteHandler();

                        } else {
                            $("#all-wrapper").addClass("finished")
                        }
                    })

                })

            }




        });




    },
    mergeArrays: function (data, votes) {
        var entries = data.variableListing;
        log(votes);
        $.each(entries, function (i, parent_val) {

            $.each(votes.variables, function (k, value) {


                if (parent_val.identifier == value.identifier) {
                    parent_val.voted = value.voted;
                    parent_val.votes = value.value;
                }

            })
        })

        log(entries);
        return entries;
    },
    initDropzone: function () {

        _dropzone = new Dropzone(".dropzone", {
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            acceptedFiles: "image/*",
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: website.translateKey("MC_REMOVE_FILE", _language),
            dictInvalidFileType: website.translateKey("MC_INVALID_FILE_TYPE", _language),
            dictDefaultMessage: website.translateKey("MC_UPLOAD_DEFAULT_MESSAGE", _language),
            url: config.rest + "image/upload/challengeImage" + _challengeIdentifier,
            headers: {
                'token': config.token
            },
            init: function () {
                _dropzone = this;

                this.on("maxfilesexceeded", function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
                this.on("error", function (file) {
                    if (!file.accepted)
                        this.removeFile(file);

                    $("#entrySubmitButton").val(website.translateKey("MC_ENTRY_SUBMIT", _language));
                    $("#entrySubmitButton").attr("disabled", false);
                    $("#myState textarea").attr("disabled", false);
                });
                this.on('success', function (file, response) {
                    //absenden mit _id comment und wahlweise image
                    var comment = $("#myState textarea").val();

                    website.restCall("variable/write/challengeComment" + _challengeIdentifier, comment, function (data) {
                        $("#myState .entry-image ").html("<a class='to_modal ' data-toggle='" + config.mediaUrl + "" + response.mediaReference + "/1000/1000'><img class='img-responsive'   src='" + config.mediaUrl + "" + response.mediaReference + "/700/320/false/true' /></a>");

                        $(".to_modal").on("click", function () {
                            console.log("Model image");
                            var src = $(this).attr("data-toggle");
                            $(".modalImg").attr("src", src);
                            $("#pictureModal").modal();
                        })

                        $(".open").remove();
                        $(".posted").show();

                        //callback
                        website.setUserPosting(comment, 0, "");
                        if (_challengeIdentifier > 20) {
                            $("#challengeOverview").addClass("own");
                        }
                    });
                });

            }




        });

    },
    setPosting: function (comment, votes, image) {


        //restCall to decide if should post

        if (!_posted) {

            if (_challengeType == "text") {
                $(".upload").hide();
            }


            $("#entrySubmitButton").click(function (e) {

                e.preventDefault();
                var comment = $("#myState textarea").val();
                var image_count = _dropzone.files.length;


                if (_challengeType == "text" && comment == "") {
                    $.Zebra_Dialog("<span class='message with-linebreaks'>" + website.translateKey("MC_FILL_IN_COMMENT", _language) + "</span>", {
                        "type": "warning",
                        "title": "PathMate2"
                    });
                    return false;
                }

                if (_challengeType == "image" && image_count <= 0) {
                    $.Zebra_Dialog("<span class='message with-linebreaks'>" + website.translateKey("MC_CHOOSE_FILE", _language) + "</span>", {
                        "type": "warning",
                        "title": "PathMate2"
                    });
                    return false;
                }


                $.Zebra_Dialog("<span class='message with-linebreaks'>" + website.translateKey("MC_CONFIRM_POSTING", _language) + "</span>", {
                    "type": "confirmation",
                    "title": "PathMate2",
                    'buttons': [{
                        caption: website.translateKey("MC_YES", _language),
                        callback: function () {
                            $("#entrySubmitButton").val(website.translateKey("MC_WAIT", _language));
                            $("#entrySubmitButton").attr("disabled", true);
                            $("#myState textarea").attr("disabled", true);

                            website.restCall("credits/storeCredit/points/po" + _challengeIdentifier, null, null);

                            if (_challengeType == "image") {
                                _dropzone.processQueue();
                            } else {
                                //absenden mit _id comment und wahlweise image
                                website.restCall("variable/write/challengeComment" + _challengeIdentifier, comment, function (data) {
                                    $(".open").remove();
                                    $(".posted").show();
                                    //callback
                                    website.setUserPosting(comment, 0, "");
                                    if (_challengeIdentifier > 20) {
                                        $("#challengeOverview").addClass("own");
                                    }
                                });
                            }
                        }
                        }, {
                        caption: website.translateKey("MC_NO", _language),
                        callback: function () {

                        }
                        }]
                })





            })
        } else {
            //already posted

            website.setUserPosting(comment, votes, image);
        }

    },
    setUserPosting: function (comment, count, url) {
        $(".posted .entry-text").html(comment);


        $(".posted .count").html(count);


        if (url != "") {
            $(".posted .entry-image").html("<a class='to_modal ' data-toggle='" + config.mediaUrl + "" + url + "/1000/1000'><img class='img-responsive'   src='" + config.mediaUrl + "" + url + "/750/300/false/true' /></a>");

            $(".to_modal").on("click", function () {
                console.log("Model image");
                var src = $(this).attr("data-toggle");
                $(".modalImg").attr("src", src);
                $("#pictureModal").modal();
            })

        } else {

        }

    },
    setStatus: function (status, is_posted) {

        // TEST
        //status= 2;

        if (status == 0) {
            $(".entry-wrapper").remove();

            _status = "active";
            _posted = false;

            if (is_posted) {
                _posted = true;
            }
        } else if (status == 1) {
            $(".entry-wrapper").remove();

            _status = "checking";
            _posted = true;
        } else if (status == 2) {
            $(".entry-wrapper #title").html(website.translateKey("MC_ENTRIES_TITLE_ACTIVE", _language));

            _status = "voting";
            _posted = true;
        } else {
            $(".entry-wrapper #title").html(website.translateKey("MC_ENTRIES_TITLE_FINISHED", _language));

            _status = "finished";
            _posted = true;
        }



        if (status == 0 && !_posted) {
            //active

            $(".open").show();
            $(".posted").hide();
        } else {
            //closed

            $(".open").hide();
            $(".posted").show();
        }


    },
    setVoteHandler: function () {


        $(".vote").on("click", function () {

            var that = this;
            if ($(this).hasClass("voted")) {
                var id = $(this).closest(".entry").attr("id");
                //restCall
                website.restCall("voting/unvote/challengeVotes" + _challengeIdentifier + "/" + id, null, function () {
                    $(that).removeClass("voted").attr("src", "img/thumb.png");
                    var vote_el = $(that).closest(".entry").find(".count");
                    var old_count = $(vote_el).text();
                    var new_count = parseInt(old_count) - 1;
                    $(vote_el).text(new_count);
                })




            } else {

                var that = this;
                var id = $(this).closest(".entry").attr("id");
                // restCall for credits
                if (!_voted && _challengeIdentifier < 10) {
                    website.restCall("credits/storeCredit/points/vt" + _challengeIdentifier, null, function () {
                        _voted = true;
                    });
                }

                //restCall
                website.restCall("voting/vote/challengeVotes" + _challengeIdentifier + "/" + id, null, function () {
                    $(that).addClass("voted").attr("src", "img/thumb_active.png");
                    var vote_el = $(that).closest(".entry").find(".count");
                    var old_count = $(vote_el).text();
                    var new_count = parseInt(old_count) + 1;
                    $(vote_el).text(new_count);
                })




            }


        })
    },
    setEntries: function (entries) {

        if (_status == "finished") {

            //sort

            entries.sort(function (a, b) {

                if (a.votes == b.votes) {
                    return 0;
                }
                if (a.votes > b.votes) {

                    // if b should come after a, return -1
                    return -1;
                } else {

                    return 1;
                }

            })


            var _count = 0;
            $.each(entries, function (i, entry) {
                if (_count < 3) {
                    var inserted = website.addEntry(entry, "");
                    
                    if (inserted) {
                        _count++;
                    }
                } else {
                    return;
                }
            })

        } else {

            $.each(entries, function (i, entry) {


                //active

                console.log(entry);

                if ($("#" + entry.identifier).length > 0) {
                    var el = $("#" + entry.identifier);
                    website.addEntry(entry, el);
                } else if (_currEntries <= _maxEntries) {
                    website.addEntry(entry, "");
                }






            });
        }
    },
    addEntry: function (entry, el) {

        if (_challengeType == "text" && entry.variables[1].value == "1" || _challengeType == "image" && entry.variables[2].value == "1") {
            return false;
        }

        if (_challengeType == "text" && entry.variables[0].value == "") {
            return false;

        }

        if (_challengeType == "image" && entry.variables[1].value == "") {
            return false;

        }

        if (entry.ownValue && _status != "finished") {
            return false;

        }

        if ($("#noEntries").length > 0) {
            $("#noEntries").remove();
        }





        var html = "";

        if (el == "") {
            html += ' <div class="col-md-4">';

            if (entry.variables.length > 2) {

                html += '             <div class="entry-image">';
                html += '             <a class="to_modal" data-toggle="' + config.mediaUrl + entry.variables[1].value + '/900/900" >';
                html += '             <img class="img-responsive" src="' + config.mediaUrl + entry.variables[1].value + '/750/320/false/true" />';
                html += '             </a>';
                html += '             </div>';
            }

            html += '            <div class="entry" id="' + entry.identifier + '">';
            html += '                <div class="likes">';
            if (entry.voted) {
                html += '                   <img class="vote voted" src="img/thumb_active.png" />';
            } else {
                html += '                   <img class="vote " src="img/thumb.png" />';
            }
            html += '                  <div class="count">' + entry.votes + '</div>';
            html += '              </div>';
            html += '              <div class="entry-text">';
            html += entry.variables[0].value;
            html += '             </div>';
            html += '         </div>';


            html += '     </div>';
        }
        if (el != "") {

            log(entry.votes);
            log(el);
            $("#" + entry.identifier).find(".count").html(entry.votes);
            //
        } else {
            _currEntries++;
            $(".entry-wrapper").append(html);
        }

        $(".to_modal").on("click", function () {
            console.log("Model image");
            var src = $(this).attr("data-toggle");
            $(".modalImg").attr("src", src);
            $("#pictureModal").modal();
        });
        
        return true;
    },
    initProfile: function () {
        /*
        if ($(".profile").length <= 0) {
            return false;
        }
        */

        website.restCall("variable/readMany/participantName,labelGroup,kanton", null, function (data) {


            $(".username").html(data.variables[0].value);

            website.adjustPrices(data.variables[2].value);
            website.initSlider();

            website.restCall("variable/calculateGroupAverage/points", null, function (points) {
                log(points);

                website.setGroupInfo(points.average * config.pointsMultiplicator, data.variables[1].value);
                website.setRank(parseInt(points.valueOfParticipant) * config.pointsMultiplicator);
            })

        });

        var videosToCheck = "";
        var videosToCheckCount = 22;

        for (var i = 1; i <= videosToCheckCount; i++) {
            if (i < 10) {
                videosToCheck += "videoShared0" + i
            } else {
                videosToCheck += "videoShared" + i
            }
            if (i < videosToCheckCount) {
                videosToCheck += ",";
            }
        }

        var alreadyAddedAVideo = false;
        website.restCall("variable/readMany/" + videosToCheck, null, function (data) {
            for (var i = 1; i <= videosToCheckCount; i++) {
                if (data.variables[i - 1].value == 1) {
                    if (!alreadyAddedAVideo) {
                        $(".video-inner-wrapper").text("");
                        alreadyAddedAVideo = true;
                    }
                    website.addVideoContent("https://data.r4l.swiss/video_" + i + "_" + _lang + ".mp4", "https://data.r4l.swiss/image_video_" + i + "_" + _lang + ".mp4.jpg");
                }
            }
        });
    },
    initSlider: function () {

        $('#sponsorCarousel').slick({
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    },
    setGroupInfo: function (points, name) {
        $(".rank_2").height(2 * Math.floor(points));
        $(".my-group").text(name);
    },
    setRank: function (points) {
        if (points > 100) {
            points = 100;
        }

        if (points >= 75) {
            //gold 
            $(".medal").attr("src", "img/medal_gold.png");

            if ($(".credit-number").length > 0) {
                $(".credit-number").addClass("gold-back");
                $("#myState .add-info").text(website.translateKey("MC_MAX_RANK_INFO", _language));
            }


        } else if (points >= 50) {
            //silver
            $(".medal").attr("src", "img/medal_silver.png");

            if ($(".credit-number").length > 0) {
                $(".credit-number").addClass("silver-back");
                $(".medal-small").attr("src", "img/medal_gold.png");
                $("#myState .add-info #beginning-text").text(website.translateKey("MC_POINTS_TO_STATUS_BEGINNING", _language).replace("#", 75 - points));
            }
        } else if (points >= 25) {
            //bronce
            $(".medal").attr("src", "img/medal_bronce.png");

            if ($(".credit-number").length > 0) {
                $(".credit-number").addClass("bronce-back");
                $(".medal-small").attr("src", "img/medal_silver.png");
                $("#myState .add-info #beginning-text").text(website.translateKey("MC_POINTS_TO_STATUS_BEGINNING", _language).replace("#", 50 - points));
            }
        } else {
            $(".medal").remove();

            if ($(".credit-number").length > 0) {
                $(".credit-number").addClass("no-back");
                $("#myState .add-info #beginning-text").text(website.translateKey("MC_POINTS_TO_STATUS_BEGINNING", _language).replace("#", 25 - points));
            }
            $(".medal-small").attr("src", "img/medal_bronce.png");
        }
        if ($(".credit-number").length > 0) {
            $(".credit-number").text(points);
            $(".rank_1").height(2 * points);
        }


    },
    adjustPrices: function (kanton) {
        for (var i = 1; i <= prices[kanton]; i++) {
            $("#sponsorCarousel").append('<div class="item"><img src="img/preise/' + kanton + '/' + i + '.jpg" class="img-responsive" alt="" title=""></div>');
        }
    },
    addVideoContent: function (video, poster) {
        log("Add video " + video + " with poster " + poster);

        $(".video-inner-wrapper").append('<div class="col-sm-4"><div class="responsive-video"><video src="' + video + '" preload="none" poster="' + poster + '" controls/></div></div>');
    },
    layout: function (value) {
        return value == config.layout;
    },
    check: function (value) {
        if ($.inArray(value, config.cleanAttributes) > -1) {
            return true;
        } else {
            return false;
        }
    },
    checkStart: function (value) {
        var evaluationResult = false;
        $.each(config.cleanAttributes, function (i, compareValue) {
            if (compareValue.indexOf(value) == 0) {
                evaluationResult = true;
            }
        });
        return evaluationResult;
    },
    getRestOfParameterBeginningWith: function (value) {
        var evaluationResult = "";
        $.each(config.cleanAttributes, function (i, compareValue) {
            if (compareValue.indexOf(value) == 0) {
                evaluationResult = compareValue.substr(value.length);
            }
        });
        return evaluationResult;
    },
    activate: function (clazz) {
        $("#all-wrapper").addClass(clazz);
    },
    allowNext: function (forceTrue) {
        log("Allow next check...");
        forceTrue = (typeof forceTrue === "undefined") ? false : forceTrue;
        if (forceTrue && !config.isLastSlide) {
            if (!$("#all-wrapper").hasClass("button-next")) {
                $("#all-wrapper").addClass("button-next");
            }
            return;
        }

        if (!config.isLastSlide) {
            var nextAllowed = true;
            $(".result_variables").each(function (index) {
                if ($(this).val() == "") {
                    nextAllowed = false;
                }
            });
            // Special case
            if (website.check("type-text-1-multi")) {
                var completeValue = 0;
                $(".result_variables").each(function (index) {
                    completeValue += 1 * $(this).val();
                });
                if (completeValue < 3 || completeValue > 3) {
                    nextAllowed = false;
                }
            }

            if (nextAllowed) {
                if (!$("#all-wrapper").hasClass("button-next")) {
                    $("#all-wrapper").addClass("button-next");
                }
                log("Yes");
            } else {
                if ($("#all-wrapper").hasClass("button-next")) {
                    $("#all-wrapper").removeClass("button-next");
                }
                log("No");
            }
        } else {
            log("No, it's last slide");
        }
    },
    disallowNext: function () {
        log("Disallow next...");
        $("#all-wrapper").removeClass("button-next");
    }

};
// Click manager
var button = {
    set: function (object, value, index) {


        index = (typeof index === "undefined") ? 0 : index;
        log("Set value " + value);
        $(object).parent().parent().children().children().removeClass("checked");
        $(object).addClass("checked");
        if (index == 0) {
            $("#result_variable_1").val(value);
        } else {
            $("#result_variable_" + index).val(value);
        }

        website.allowNext();
    },
    setMany: function (object, value) {
        $("#result_variable_1").val("");
        if ($(object).hasClass("checked")) {
            $(object).removeClass("checked");
        } else {
            $(object).addClass("checked");
        }
        var result_var = [];
        $(object).closest(".select-many").find(".checked").each(function (i, value) {

            result_var.push($(value).attr("id"));
        })

        $("#result_variable_1").val(result_var.join());
        website.allowNext();
    },
    switch: function (object, value, index) {
        index = (typeof index === "undefined") ? 0 : index;
        var currentVal;
        if (index == 0) {
            currentVal = $("#result_variable_1").val();
        } else {
            currentVal = $("#result_variable_" + index).val();
        }

        if (currentVal == 0) {
            log("Set value " + value);
            $(object).addClass("checked");
            if (index == 0) {
                $("#result_variable_1").val(value);
            } else {
                $("#result_variable_" + index).val(value);
            }
        } else {
            log("Set value 0");
            $(object).removeClass("checked");
            if (index == 0) {
                $("#result_variable_1").val(0);
            } else {
                $("#result_variable_" + index).val(0);
            }
        }

        website.allowNext();
    },
    text: function (object, onlyNumbers) {
        var textfield = $(object);
        var text = $(object).val();
        log("Text change '" + object.id + "' -> '" + text + "'");
        $("#result_variable_1").val(text);
        if (text.length == 0 && textfield.hasClass("correct")) {
            textfield.removeClass("correct");
            website.disallowNext();
        } else if (text.length > 0 && !textfield.hasClass("correct")) {
            textfield.addClass("correct");
            website.allowNext();
        }
    },
    drinksText: function (object, id) {
        log("Drinks text change " + object.id);
        var textfield = $(object);
        var textfieldParent = $(object).parent();
        var text = $(object).val();
        if (text.length == 0 && textfieldParent.hasClass("correct")) {
            textfieldParent.removeClass("correct");
        } else if (text.length > 0 && !textfieldParent.hasClass("correct")) {
            textfieldParent.addClass("correct");
        }
        if ($(".drinks-box.drinks-1").hasClass("correct") && $(".drinks-box.drinks-2").hasClass("correct") && $(".drinks-box.drinks-3").hasClass("correct") && $(".drinks-box.drinks-4").hasClass("correct") && $(".drinks-box.drinks-5").hasClass("correct") && $(".drinks-box.drinks-6").hasClass("correct") && $(".drinks-box.drinks-7").hasClass("correct")) {
            website.allowNext();
        } else {
            website.disallowNext();
        }

        var value = text;
        var values = $("#result_variable_1").val().split(",");
        values[id - 1] = value;
        $("#result_variable_1").val(values.join());
    },
    validate: function (buttonEvent) {
        var charCode = (buttonEvent.which) ? buttonEvent.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    },
    minus: function (id) {
        var values = $("#result_variable_1").val().split(",");
        var value = values[id - 1];
        if (value != "?" && value > 0) {
            value--;
            values[id - 1] = value;
            $("#result_variable_1").val(values.join());
            $("#drinks-" + (id) + " p.value").html(value);
        }
    },
    plus: function (id) {
        var values = $("#result_variable_1").val().split(",");
        var value = values[id - 1];
        if (value == "?") {
            value = -1;
        }

        if (value < 99) {
            value++;
            values[id - 1] = value;
            $("#result_variable_1").val(values.join());
            $("#drinks-" + (id) + " p.value").html(value);
        }

        log($("#result_variable_1").val());
        for (var i in values) {
            if (values[i] == "?") {
                return;
            }
        }

        website.allowNext();
    },
    previous: function () {
        $("#form_previous").submit();
    },
    next: function () {
        log("Next slide...");
        $("#form_next").submit();
    },
    submit: function () {
        $("#form_submit").submit();
    },
    keyNext: function (event) {
        var pressedKey = event ? event.which : window.event.keyCode;
        if (pressedKey == 13) {
            log("Enter has been pressed");
            if ($("#all-wrapper").hasClass("button-next")) {
                if ($("#myState .content textarea").val() != "") {
                    return true;
                }
                if (config.isFeedback) {
                    button.next();
                } else {
                    button.submit();
                }
                return false;
            }
        }

        return pressedKey != 13;
    }
};
// Execute when page is loaded succesful
$(document).ready(function () {
    Dropzone.autoDiscover = false;

    website.init();
    website.translate(function () {

        if (website.check("graph")) {
            website.drawGraph();
        }
        if (website.check("profile")) {
            website.initProfile();
        }

        if (website.check("besmart")) {
            $(".filler").hide();
        }

        if (website.checkStart("challenge-")) {
            website.initChallenge();
        }

        if (website.check("trafficlight")) {
            website.fillTrafficLight();
        }

        // Show video
        if (website.check("video")) {
            if (_lang == "fr-CH") {
                $("#media-container").html("<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"//www.youtube.com/embed/E7c1vG9NYCc?autoplay=1&showinfo=0&rel=0\" allowfullscreen></iframe></div>");
            } else {
                $("#media-container").html("<div class=\"embed-responsive embed-responsive-16by9\"><iframe class=\"embed-responsive-item\" src=\"//www.youtube.com/embed/uy2JYL5imj0?autoplay=1&showinfo=0&rel=0\" allowfullscreen></iframe></div>");
            }
        }

    });


    if (website.checkStart("credits-")) {
        website.restCall("credits/storeCredit/points/" + website.getRestOfParameterBeginningWith("credits-"), null, function () {
            if (config.redirect) {
                window.location = $("a#media-link").html();
            }
        });
    }

    if (config.questionsCount <= 1) {
        $(".multi-question").html("").removeClass("multi-question");
    }

    $("#select-one-lickert-3-6 .select-one").each(function () {

        var _likert_percentage = 100 / $(this).find(".likert li").size();
        $(this).find(".likert li").width(_likert_percentage + "%");
    })


    var calculated = false;
    var t_elem;
    var t = 0;
    if (!calculated) {

        $(".select-box p").each(function () {

            if (jQuery(this).innerHeight() > t) {
                t_elem = this;

                if (!jQuery(t_elem).is(":hidden")) {

                    t = jQuery(this).innerHeight();
                }
            }



        });
        //var padding=parseInt($(t_elem).css("padding-top"));

        var height = t;
        $(".select-box p").css("height", height + "px");
        calculated = true;
    }

    $(".likert .select-box ").each(function () {
        var width = $(this).innerWidth();
        width = width + 4;

        var max_width = $(this).parent().width();

        if (width > max_width) {
            width = max_width - 20;
        }

        $(this).height(width);
        $(this).width(width);
        $(this).css("line-height", width + 2 + "px");
        $(".first-likert ").height(width + "px");
        $(".last-likert ").height(width + "px");
    });
    window.onresize = function () {
        if (website.checkStart("image-")) {
            website.eyecatcher();
        }

        var width = "";
        $(".likert .select-box ").each(function () {
            width = $(this).innerWidth();
            width = width + 4;

            var max_width = $(this).parent().width();

            if (width > max_width) {
                width = max_width - 20;
            }

            $(this).height(width);
            $(this).width(width);
            $(this).css("line-height", width + 2 + "px");

        })
        var width = $(".likert .select-box:first-child ").css("width");

        $(".first-likert").css("height", width + "px");
        $(".last-likert").css("height", width + "px");



        var calculated = false;
        var t_elem;
        var t = 0;
        if (!calculated) {

            $(".select-box p").each(function () {


                if (!jQuery(this).parent().hasClass("lickert-3-6")) {
                    $(this).css("height", "auto");

                    if (jQuery(this).innerHeight() > t) {
                        t_elem = this;

                        if (!jQuery(t_elem).is(":hidden")) {

                            t = jQuery(this).innerHeight();
                        }
                    }

                    var height = t + 20;
                    $(this).css("height", height + "px");

                }

            });
            //var padding=parseInt($(t_elem).css("padding-top"));

        };
        var t_elem;
        var t = 0;


        $(".likert .select-box p").each(function () {

            if (jQuery(this).innerHeight() > t) {
                t_elem = this;

                if (!jQuery(t_elem).is(":hidden")) {

                    t = jQuery(this).innerHeight();
                }
            }

        });
        //var padding=parseInt($(t_elem).css("padding-top"));

        var height = t;
        $(".likert .select-box p").css("height", height + "px");
        calculated = true;

    }

});